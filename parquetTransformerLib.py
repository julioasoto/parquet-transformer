# -*- coding: utf-8 -*-

import ConfigParser
from sys import argv, exit

"""Biblioteca que transforma datos en texto planos a datos con formato parquet.
Necesita un archivo de configuración en el que estén definido el esquema, así como
otras opciones. Para más ayuda, teclear en una terminal:

python parquetTransformerLib.py

O bien consultar Esquema.conf.template

El repositorio con la última versión está disponible en: 
http://bitbucket.org/julioasoto/parquet-transformer
"""

# Parseo de argumentos de la línea de comandos

if ( ("--entrada" not in argv) or ("--salida" not in argv) or ("--esquema" not in argv) ):
    print("""Uso:
    spark-submit parquetTransformerLib.py --entrada archivo_de_entrada --salida archivo_parquet_de_salida --esquema archivo_de_esquema
Donde:
    archivo_de_entrada: es la ruta al archivo de texto plano a transformar a Parquet

    archivo_de_salida:  es la ruta al directorio que contendrá los archivos Parquet
                        particionados resultantes de la transformación, así como sus 
                        metadatos. Es costumbre poner al directorio la extensión .parquet
                        al final, a pesar de que sea un directorio y no un archivo
                        como tal
    
    archivo_de_esquema: ruta al archivo que define el esquema y otras opciones a aplicar.
                        Las opciones a especificar en el archivo se pueden ver en 
                        Esquema.conf.template

Visita http://bitbucket.org/julioasoto/parquet-transformer para conseguir la última versión.
""")
    exit(2)


entrada = argv[argv.index("--entrada")+1]
salida = argv[argv.index("--salida")+1]
esquema_archivo_ruta = argv[argv.index("--esquema")+1]



# Parseo del archivo de esquema

esquema_archivo = ConfigParser.ConfigParser()
esquema_archivo.read(esquema_archivo_ruta)

secciones = esquema_archivo.sections()

if ( ("Esquema" not in secciones) or ("General" not in secciones) ):
    print("ERROR: se debe definir [General] y [Esquema] en %s" % esquema_archivo_ruta)
    exit(3)


if "separador" not in esquema_archivo.options("General"):
    print("ERROR: campo 'separador' no encontrado en %s" % esquema_archivo_ruta)
    exit(4)
separador = esquema_archivo.get("General", "separador")


modo_escritura = "error"
traducciones_modo_escritura = {"ligar": "append", "sobreescribir": "overwrite" , "ignorar": "ignore", "error": "error"}
if ("modo_escritura" not in esquema_archivo.options("General")):
    pass
else:
    modo_escritura = traducciones_modo_escritura[esquema_archivo.get("General", "modo_escritura")]

campos_nulos = True
traducciones_campos_nulos = {"true": True, "false": False}
if ("campos_nulos" not in esquema_archivo.options("General")):
    pass
else:
    campos_nulos = traducciones_campos_nulos[esquema_archivo.get("General", "campos_nulos")]


verbosidad = 5
if ("verbosidad" not in esquema_archivo.options("General")):
    pass
else:
    verbosidad = int(esquema_archivo.get("General", "verbosidad"))



lista_de_columnas = esquema_archivo.options("Esquema")
esquema_parseado = [(nombre_columna, esquema_archivo.get("Esquema", nombre_columna)) for nombre_columna in lista_de_columnas]



# Inicialización de Spark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StructField, StructType, StringType, IntegerType, DoubleType

conf = (SparkConf()
        .setAppName("Transformador de %s a %s" % (entrada, salida))
       )

sc = SparkContext(conf=conf)



# Traducción entre lo que necesitamos del archivo de esquema y la estructura de dato a aplicar

def unmodify(element):
    return element

tabla_de_traduccion_esquema = {"integer": IntegerType, "string": StringType, "double": DoubleType}
tabla_de_traduccion_datos = {"integer": int, "string": unmodify, "double": float}



# Inicio de las transformaciones

datos_crudos = sc.textFile(entrada)

datos_en_lista = datos_crudos.map(lambda fila: fila.split(separador))

if (len(datos_en_lista.first()) != len(esquema_parseado)):
    longitud_datos = len(datos_en_lista.first())
    longitud_esquema = len(esquema_parseado)
    for i in range(verbosidad):
        print("ERROR: La cantidad de campos del archivo %s es %s, mientras que la de %s es %s" % (entrada,
                                                                                                  longitud_datos,
                                                                                                  esquema_archivo_ruta,
                                                                                                  longitud_esquema)
    )
    exit(5)

datos_con_tipos = datos_en_lista.map(lambda fila: [tabla_de_traduccion_datos[tipo[1]](campo) 
                                                   for campo, tipo in zip(fila,esquema_parseado)])



# Comenzando con Spark SQL

sqlCtx = SQLContext(sc)

campos = [StructField(nombre, tabla_de_traduccion_esquema[tipo_de_dato](), nullable=campos_nulos) for nombre, tipo_de_dato in esquema_parseado]
esquema = StructType(campos)

datos_en_dataframe = sqlCtx.createDataFrame(datos_con_tipos, esquema)

datos_en_dataframe.write.parquet(salida, mode=modo_escritura)

sc.stop()
for i in range(verbosidad):
    print("SUCCESS: %s transformado a parquet en %s satisfactoriamente" % (entrada, salida))

# Parquet Transformer Lib
Biblioteca que transforma datos en texto planos a datos con formato parquet.
Necesita un archivo de configuración en el que estén definido el esquema, así como
otras opciones. Para más ayuda, teclear en una terminal:

```bash
python parquetTransformerLib.py
```

O bien consultar `Esquema.conf.template`

**Nota**: Requiere python `2.7.3` o superior para funcionar, puesto que la biblioteca `ConfigParser` comienza a usar la estructura de dato `collections.OrderedDict` a partir de dicha versión. ***No funcionará con versiones anteriores***, puesto que el programa no será capaz de parsear el orden de las columnas de forma apropiada, dando lugar a incoherencias. Si la versión de python que tienes en el `$PATH` no es compatible, necesitas instalar una versión compatible en algún directorio del sistema, y exportar la variable de entorno `PYSPARK_PYTHON=/directorio/hasta/bin/python` para que apunte al ejecutable de python adecuado.
